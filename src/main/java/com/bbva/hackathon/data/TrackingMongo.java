package com.bbva.hackathon.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.sql.Time;
import java.util.Date;

@Document("TrackingMongov1")
public class TrackingMongo {

    @Id
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre_estado() {
        return nombre_estado;
    }

    public void setNombre_estado(String nombre_estado) {
        this.nombre_estado = nombre_estado;
    }

    public String getDescripcion_estado() {
        return descripcion_estado;
    }

    public void setDescripcion_estado(String descripcion_estado) {
        this.descripcion_estado = descripcion_estado;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getCod_operacion() {
        return cod_operacion;
    }

    public void setCod_operacion(String cod_operacion) {
        this.cod_operacion = cod_operacion;
    }

    public String getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getFecha_estado_ini() {
        return fecha_estado_ini;
    }

    public void setFecha_estado_ini(String fecha_estado_ini) {
        this.fecha_estado_ini = fecha_estado_ini;
    }

    public String getHora_estado_ini() {
        return hora_estado_ini;
    }

    public void setHora_estado_ini(String hora_estado_ini) {
        this.hora_estado_ini = hora_estado_ini;
    }

    public String getFecha_estado_fin() {
        return fecha_estado_fin;
    }

    public void setFecha_estado_fin(String fecha_estado_fin) {
        this.fecha_estado_fin = fecha_estado_fin;
    }

    public String getHora_estado_fin() {
        return hora_estado_fin;
    }

    public void setHora_estado_fin(String hora_estado_fin) {
        this.hora_estado_fin = hora_estado_fin;
    }

    @Override
    public String toString() {
        return "TrackingMongo{" +
                "id=" + id +
                ", nombre_estado='" + nombre_estado + '\'' +
                ", descripcion_estado='" + descripcion_estado + '\'' +
                ", comentario='" + comentario + '\'' +
                ", cod_operacion='" + cod_operacion + '\'' +
                ", id_cliente='" + id_cliente + '\'' +
                ", fecha_estado_ini='" + fecha_estado_ini + '\'' +
                ", hora_estado_ini='" + hora_estado_ini + '\'' +
                ", fecha_estado_fin='" + fecha_estado_fin + '\'' +
                ", hora_estado_fin='" + hora_estado_fin + '\'' +
                '}';
    }

    public TrackingMongo(long id, String nombre_estado, String descripcion_estado, String comentario, String cod_operacion, String id_cliente, String fecha_estado_ini, String hora_estado_ini, String fecha_estado_fin, String hora_estado_fin) {
        this.id = id;
        this.nombre_estado = nombre_estado;
        this.descripcion_estado = descripcion_estado;
        this.comentario = comentario;
        this.cod_operacion = cod_operacion;
        this.id_cliente = id_cliente;
        this.fecha_estado_ini = fecha_estado_ini;
        this.hora_estado_ini = hora_estado_ini;
        this.fecha_estado_fin = fecha_estado_fin;
        this.hora_estado_fin = hora_estado_fin;
    }

    @NotBlank
    @Size(max=100)
    @Indexed(unique=true)

    public String nombre_estado;
    public String descripcion_estado;
    public String comentario;
    public String cod_operacion;
    public String id_cliente;
    public String fecha_estado_ini;
    public String hora_estado_ini;
    public String fecha_estado_fin;
    public String hora_estado_fin;

    }
