package com.bbva.hackathon.data;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrackingRepository extends MongoRepository<com.bbva.hackathon.data.TrackingMongo, String> {
    @Query("{'cod_operacion':?0}")
    public List<TrackingMongo> findCod_operacion(String cod_operacion);

    @Query("{'cod_operacion':?0},{'nombre_estado':?1}")
    public List<TrackingMongo> findCod_operacionEstado(String cod_operacion, String descripcion_estado);


    @Query("{'id_cliente': {$gt: ?0, $lt: ?1}}")
   public List<TrackingMongo> findByCliente(String id_cliente);
}
