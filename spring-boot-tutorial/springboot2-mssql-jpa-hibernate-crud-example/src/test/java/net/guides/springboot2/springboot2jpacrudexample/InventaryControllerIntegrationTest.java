package net.guides.springboot2.springboot2jpacrudexample;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import net.guides.springboot2.crud.Application;
import net.guides.springboot2.crud.model.Inventary;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InventaryControllerIntegrationTest {
	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

//	@Test
	public void contextLoads() {

	}

//	@Test
	public void testGetAllinventary() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/inventary",
				HttpMethod.GET, entity, String.class);
		
		assertNotNull(response.getBody());
	}

//	@Test
	public void testGetEmployeeById() {
		Inventary inventary = restTemplate.getForObject(getRootUrl() + "/inventary/1", Inventary.class);
		System.out.println(inventary.getnombre());
		assertNotNull(inventary);
	}

//	@Test
	public void testCreateEmployee() {
		Inventary inventary = new Inventary();
		inventary.setnombre("Play");
		inventary.setdescripcion("Consola");
		inventary.setprecio(120.12);
		inventary.setcantidad(1000);
		inventary.seturlimagen("https://www.alkosto.com/consola-ps4-megapack-15-1-tera-1-control-3-juegos-spider-man-edicion-horizon-zero-dawn-edicion-completa-y-ratchet-clank-suscripcion-de-3-meses-a-playstation-plus");
		inventary.setnombreimagen("play4.jpg");
		inventary.setcategoria("Consolas");



		ResponseEntity<Inventary> postResponse = restTemplate.postForEntity(getRootUrl() + "/inventary", inventary, Inventary.class);
		assertNotNull(postResponse);
		assertNotNull(postResponse.getBody());
	}

//	@Test
	public void testUpdateEmployee() {
		int id = 1;
		Inventary inventary = restTemplate.getForObject(getRootUrl() + "/inventary/" + id, Inventary.class);
		inventary.setnombre("Play 4");
		inventary.setdescripcion("Consola de video Juego");;

		restTemplate.put(getRootUrl() + "/inventary/" + id, inventary);

		Inventary updatedInventary = restTemplate.getForObject(getRootUrl() + "/inventary/" + id, Inventary.class);
		assertNotNull(updatedInventary);
	}

//	@Test
	public void testDeleteEmployee() {
		int id = 2;
		Inventary inventary = restTemplate.getForObject(getRootUrl() + "/inventary/" + id, Inventary.class);
		assertNotNull(inventary);

		restTemplate.delete(getRootUrl() + "/inventary/" + id);

		try {
			inventary = restTemplate.getForObject(getRootUrl() + "/inventary/" + id, Inventary.class);
		} catch (final HttpClientErrorException e) {
			assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
	}
}
