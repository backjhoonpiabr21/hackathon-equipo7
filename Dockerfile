FROM openjdk:8-jdk-alpine
COPY target/hackathon-0.0.1-SNAPSHOT.jar Operaciones.jar
EXPOSE 8084
ENTRYPOINT ["java", "-jar", "/Operaciones.jar"]